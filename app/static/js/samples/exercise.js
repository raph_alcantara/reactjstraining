var Item = React.createClass({
  addToCart: function(e) {
    e.preventDefault()
    var form = e.target;
    var item_name = form.item_name.value.trim();
    var item_price = form.price.value.trim();
    var item_qty = form.qty.value.trim();
    var item_total = item_price * item_qty;

    if (!item_qty) {
      return;
    }

    console.log(this.props.onCartSubmit)
    this.props.onCartSubmit({item_name: item_name, price: item_price, qty: item_qty, total: item_total});

    form.qty.value = ''

 
    // this.props.saveValues(item_data)
    console.log(item_name)
    console.log(item_price)
    console.log(item_qty)
    console.log(item_total)
    return;
  },

  render: function() {
    return (
      <div>
          <form onSubmit={this.addToCart}>
          {this.props.item_name}   {this.props.price}
          <input type="hidden" name="item_name" ref="item_name" value={this.props.item_name} />
          <input type="hidden" name="price" ref="price" value={this.props.price} />
          <input type="text" name="qty" ref="qty" placeholder="QTY" />
          <input type="submit" value="add to cart" />
          </form>
      </div>
    );
  }
});

var Cart = React.createClass({
  render: function() {
    return (
      <div class="row">
          <table class="table">
            <tr>
              <th>Item Name</th>
              <th>Price</th>
              <th>Qty</th>
              <th>Total</th>
            </tr>
            <tr>
              <td>{this.props.item_name}</td>
              <td>{this.props.price}</td>
              <td>{this.props.qty}</td>
              <td>{this.props.total}</td>
            </tr>
          </table>
      </div>
    );
  }

});

var ItemList = React.createClass({
  something: function(item) {
    this.props.cartHandler(item)
  },
  render: function() {
    var itemNodes = this.props.data.map(function (items, index) {
      return (
        <Item key={index} item_name={items.item_name} price={items.price} onCartSubmit={this.something} />
      );
    }.bind(this));
    return (
      <div>
        {itemNodes}
      </div>
    );
  }
});

var CartList = React.createClass({
  render: function() {
    var cartNodes = this.props.data.map(function (cart, index) {
      return (
        <Cart key={index} item_name={cart.item_name} price={cart.price} qty={cart.qty} total={cart.total} />
      );
    });
    return (
      <div>
        {cartNodes}
      </div>
    );
  }
});

var ItemForm = React.createClass({
  handleSubmit: function(e) {
    e.preventDefault();
    var form = e.target;
    var item_name = form.item_name.value.trim();
    var item_price = form.price.value.trim();
    if (!item_price || !item_name) {
      return;
    }
    this.props.onCommentSubmit({item_name: item_name, price: item_price});
    // TODO: send request to the server
    form.item_name.value = '';
    form.price.value = '';
    return;
  },
  render: function() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input type="text" placeholder="Item Name" name="item_name"/>
        <input type="text" placeholder="Price" name="price"/>
        <input type="submit" value="Add item" />
      </form>
    );
  }
});

var PostBox = React.createClass({
  loadCommentsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        var postDict = JSON.parse(JSON.stringify(data));   
        this.setState({data: postDict['posts']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  handleCommentSubmit: function(comment) {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      type: 'POST',
      data: comment,
      success: function(data) {
        var postDict = JSON.parse(JSON.stringify(data));   
        this.setState({data: postDict['posts']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  handleCartSubmit: function(comment) {
    $.ajax({
      url: this.props.cart_url,
      dataType: 'json',
      type: 'POST',
      data: comment,
      success: function(data) {
        var postDict = JSON.parse(JSON.stringify(data));   
        this.setState({cart: postDict['posts']});
        this.setState({cart_total: postDict['total']});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.cart_url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: [], cart: [], cart_total: []};
  },
  componentDidMount: function() {
    this.loadCommentsFromServer();
    setInterval(this.loadCommentsFromServer, this.props.pollInterval);
  },
  render: function() {
    return (
      <div>
        <h1>Items</h1>
        <ItemList data={this.state.data} cartHandler={this.handleCartSubmit} />
        <br/>
        <br/>
        <br/>
        <br/>
        <ItemForm onCommentSubmit={this.handleCommentSubmit}/>
        <br/>
        <CartBox cart={this.state.cart} />
        <h3>cart total: {this.state.cart_total}</h3>
      </div>
    );
  }
});

var CartBox = React.createClass({
  render: function() {
    return (
      <div>
        <h1>Cart</h1>
        <CartList data={this.props.cart} />
        <br/>
        <br/>
      </div>
    );
  }
});