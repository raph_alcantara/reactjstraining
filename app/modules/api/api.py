from flask import Blueprint
from flask import jsonify
from flask import request

mod = Blueprint('api', __name__)

sample_data = [
        {'author':'Ali', 'post':'Lorem ipsum'},
        {'author':'Ruth', 'post':'Dolor sit amet'},
        {'author':'Andrew', 'post':'Consectetur Adipiscing Elit'},
    ]

exercise_data = [
        {'item_name':'A', 'price':'5'},
    ]

cart_data = []
cart_total = [
        {'cart_total':int(0)},
    ]

@mod.route('/getsampledata', methods=['GET','POST'])
def get_sample_data():
    print request.form.keys()
    if request.method=='POST':
        sample_data.append({
            'author':request.form['author'], 
            'post':request.form['post'], 
        })
    return jsonify(posts=sample_data)

@mod.route('/exercisedata', methods=['GET','POST'])
def get_exercise_data():
    print request.form.keys()
    if request.method=='POST':
        exercise_data.append({
            'item_name':request.form['item_name'], 
            'price':request.form['price'], 
        })
    return jsonify(posts=exercise_data)

@mod.route('/cartdata', methods=['GET','POST'])
def get_cart_data():
    print request.form.keys()
    total = 0
    if request.method=='POST':

        total = int(request.form['qty']) * int(request.form['price'])
        if(len(cart_data) == 0):
            print('fasdfasd')
            cart_data.append({
                'item_name':request.form['item_name'],
                'price':int(request.form['price']),
                'qty':int(request.form['qty']),
                'total':int(total),
            })
        else:
            flag = 0
            for data in cart_data:
                if(data['item_name'] == request.form['item_name']):
                    flag = 1
                    data['qty'] += int(request.form['qty'])
                    data['total'] = int(request.form['price']) * int(data['qty'])
                    break

            if(flag == 0):
                cart_data.append({
                    'item_name':request.form['item_name'],
                    'price':int(request.form['price']),
                    'qty':int(request.form['qty']),
                    'total':int(total),
                })

        num = 0
        for x in cart_data:
            num += x['total']
        for i in cart_total:
            i['cart_total'] = int(num)
        print(num)


    return jsonify(posts=cart_data, total=cart_total)
